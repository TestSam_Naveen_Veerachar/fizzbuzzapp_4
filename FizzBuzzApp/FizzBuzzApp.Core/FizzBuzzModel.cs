﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using PagedList;


namespace FizzBuzzApp.Core
{
    public class FizzBuzzModel
    {
        /// <summary>
        /// Data Validations using Range Validators.
        /// Required field validators.
        /// </summary>
        [Range(1, 1000)] //To restrict the entry between 1 and 1000.
        [Required]
        [DisplayName("Value")] // To inform the user to enter value with error message.
        public int Num { get; set; }
        /// <summary>
        /// Using paged list MVC
        /// </summary>
        public IPagedList<String> FizzBuzzNumbers { get; set; }
    }
}
