﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using FizzBuzzApp.Infrastructure;

namespace FizzBuzzApp.Test
{
    [TestFixture]
    public class FizzBuzzAppTest 
    {
        /// <summary>
        /// Creating Temporary DateTime of Wednesday and NotWednesday
        /// </summary>
        private readonly DateTime _notWednesday = new DateTime(2017, 11, 14);
        private readonly DateTime _wednesday = new DateTime(2017, 11, 15);

        /// <summary>
        /// If Enter Number is one it should return 1.
        /// And it should be not wednesday
        /// </summary>
        [Test]
        public void If_enter_number_is_one_it_should_return_1()
        {
            //Arrange
            var fizbuzz = new FizzBuzzAppInfra {Today = _notWednesday};
            var expected = new List<string> { "1" };
            //Act
            var actual = fizbuzz.GetFizzBuzz(1);
            //Assert
            CollectionAssert.AreEqual(expected, actual);

        }
        /// <summary>
        /// If Enter Number is one and two it should return 1 and 2.
        /// And it should be not wednesday
        /// </summary>
        [Test]
        public void GetFizzBuzzIf_enter_number_is_one_Two_it_should_return_one_Two()
        {
            //Arrange
            var fizbuzz = new FizzBuzzAppInfra {Today = _notWednesday};
            var expected = new List<string> { "1", "2" };
            //Act
            var actual = fizbuzz.GetFizzBuzz(2);
            //Assert
            CollectionAssert.AreEqual(expected, actual);
        }
        /// <summary>
        /// If input is three it should return one Two Fizz
        /// And it should be not wednesday
        /// </summary>
        [Test]
        public void GetFizzBuzz_If_input_is_three_it_should_return_one_Two_Fizz()
        {
            // Arrange
            var fizbuzz = new FizzBuzzAppInfra {Today = _notWednesday};
            var expected = new List<string> { "1", "2", "Fizz" };
            //Act
            var actual = fizbuzz.GetFizzBuzz(3);
            //Assert
            CollectionAssert.AreEqual(expected, actual);
        }
        /// <summary>
        ///  If input is three it should return one Two Wizz
        /// And it should be wednesday
        /// </summary>
        [Test]
        public void GetFizzBuzz_Should_Return_List_With_One_Two_Wizz_If_Input_Is_Three_And_Is_Wednesday()
        {
            // Arrange
            var fizbuzz = new FizzBuzzAppInfra {Today = _wednesday};
            var expected = new List<string> { "1", "2", "Wizz" };

            // Act
            var actual = fizbuzz.GetFizzBuzz(3);

            // Assert
            CollectionAssert.AreEqual(expected, actual);

        }
        /// <summary>
        /// If input is Five it should return 1,2,Fizz,4,Buzz
        /// And it should not wednesday.
        /// </summary>
        [Test]
        public void GetFizzBuzz_If_Input_is_Five_and_if_it_is_notwednesday()
        {
            // Arrange
            var fizbuzz = new FizzBuzzAppInfra {Today = _notWednesday};
            var expected = new List<string> { "1", "2", "Fizz", "4", "Buzz" };

            // Act
            var actual = fizbuzz.GetFizzBuzz(5);

            // Assert
            CollectionAssert.AreEqual(expected, actual);

        }
        /// <summary>
        /// If input is Five it should return 1,2,Wizz,4,Wuzz
        /// And it should wednesday.
        /// </summary>
        [Test]
        public void GetFizzBuzz_If_Input_is_Five_and_it_is_wednesday()
        {
            //Arrange
            var fizbuzz = new FizzBuzzAppInfra {Today = _wednesday};
            var expected = new List<string> { "1", "2", "Wizz", "4", "Wuzz" };
            //Act
            var actual = fizbuzz.GetFizzBuzz(5);
            //Assert
            CollectionAssert.AreEqual(expected, actual);

        }
    }
}
