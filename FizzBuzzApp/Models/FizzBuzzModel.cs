﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using PagedList;

namespace FizzBuzzApp.Models
{
    /// <summary>
    /// Fizz buzz model class.
    /// </summary>
    public class FizzBuzzModel
    {
        /// <summary>
        /// Number.
        /// Data Validations using Range Validators.
        /// Required field validators.
        /// </summary>
        
        [Range(1, 1000)] 
        [Required]
        [DisplayName("Value")] 
        public int Number { get; set; }

        /// <summary>
        /// Using paged list MVC
        /// </summary>
        public IPagedList<String> FizzBuzzNumbers { get; set; }
    }
}
