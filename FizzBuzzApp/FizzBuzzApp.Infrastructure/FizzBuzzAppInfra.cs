﻿using System;
using System.Collections.Generic;

namespace FizzBuzzApp.Infrastructure
{
    public class FizzBuzzAppInfra : IFizzBuzzAppInfra
    {
        /// <summary>
        /// Calling from IFizzBuzzAppInfra Repository.
        /// To compare the todays date is wednesday or not.
        /// </summary>
        public FizzBuzzAppInfra()
        {
            Today = DateTime.Now;
        }

        public DateTime Today { get; set; }
        /// <summary>
        /// To check the condition True or False.
        /// </summary>
        public bool IsError { get; set; }
        /// <summary>
        /// To show the error response.
        /// </summary>
        public string ResponseMessage { get; set; }

        /// <summary>
        /// Get the number eneter number in the textbox to validate if it is greater than 1000 throw exception.
        /// Else check which are divisible by 3,5 and both 3,5
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public List<string> GetFizzBuzz(int value)
        {
            var numbers = new List<string>();
            if (value < 1 || value > 1000)
            {
                IsError = true;
                ResponseMessage = "The Value should not be less than 0 and more than 1000";
                return numbers;
            }
            for (var i = 1; i <= value; i++)
            {
                numbers.Add(GetFizzBuzzValue(i));
            }
            return numbers;
        }
        /// <summary>
        /// To get the result by according to the condition below.
        /// Get the number eneter number in the textbox to validate if it is greater than 1000 throw exception.
        /// Else check which are divisible by 3,5 and both 3,5
        /// </summary>
        /// <param name="i"></param>
        /// <returns>Fizz,Buzz,FizzBuzz,Wizz,Wuzz and WizzWuzz</returns>
        private string GetFizzBuzzValue(int i)
        {
            if (i % 3 == 0 && i % 5 == 0)
            {
                if (Today.DayOfWeek == DayOfWeek.Wednesday)
                {
                    return "WizzWuzz";
                }
                return "FizzBuzz";
            }
            if (i % 3 == 0)
            {
                if (Today.DayOfWeek == DayOfWeek.Wednesday)
                {
                    return "Wizz";
                }
                return "Fizz";
            }
            if (i % 5 == 0)
            {
                if (Today.DayOfWeek == DayOfWeek.Wednesday)
                {
                    return "Wuzz";
                }
                return "Buzz";
            }
            return i.ToString();
        }
    }
}
