﻿using System;
using System.Collections.Generic;

namespace FizzBuzzApp.Infrastructure
{
    public interface IFizzBuzzAppInfra
    {
        DateTime Today { get; set; }
        bool IsError { get; set; }
        string ResponseMessage { get; set; }
        List<string> GetFizzBuzz(int value);
    }
}
