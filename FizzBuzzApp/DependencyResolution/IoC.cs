// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IoC.cs" company="Web Advanced">
// Copyright 2012 Web Advanced (www.webadvanced.com)
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------


using FizzBuzzApp.Infrastructure;
using FizzBuzzApp.Infrastructure.Rules;
using FizzBuzzApp.Infrastructure.Rules.DayandWeekConditions;
using FizzBuzzApp.Infrastructure.Rules.DayandWeekConditions.Def;
using FizzBuzzApp.Infrastructure.Rules.DayandWeekConditions.Interface;
using FizzBuzzApp.Infrastructure.Rules.DayandWeekConditions.Stratergies;
using StructureMap;
using StructureMap.Graph;
using FizzBuzzApp.Infrastructure.Rules.FizzBuzzRules.Def;
using FizzBuzzApp.Infrastructure.Rules.FizzBuzzRules.FizzBuzzConditions;
using FizzBuzzApp.Infrastructure.Rules.Interface;

namespace FizzBuzzApp.DependencyResolution
{
    /// <summary>
    /// IoC class.
    /// </summary>
    public static class IoC
    {
        /// <summary>
        /// Initialize Method
        /// </summary>
        /// <returns>ObjectFactory container.</returns>
        public static IContainer Initialize()
        {
            ObjectFactory.Initialize(x =>
                        {
                            x.Scan(scan =>
                                    {
                                        scan.TheCallingAssembly();
                                        scan.WithDefaultConventions();
                                    });
                            x.For<IFizzBuzzAppInfra>().Use<FizzBuzzAppInfra>();

                            x.For<IDivisible>().Use<FizzCondition>();

                            x.For<IDivisible>().Use<BuzzCondition>();

                            x.For<IDivisible>().Use<FizzAndBuzz>();

                            x.For<IDayOfTheWeekCondition>().Use<ConditionForWed>().Ctor<IDayOfTheWeek>();

                            x.For<IDayOfTheWeek>().Use<DayOfTheWeek>();

                        });
            return ObjectFactory.Container;
        }

       
    }
}