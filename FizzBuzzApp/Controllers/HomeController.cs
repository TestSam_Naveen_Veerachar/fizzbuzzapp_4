﻿using System.Web.Mvc;
using FizzBuzzApp.Infrastructure.Rules.Interface;
using FizzBuzzApp.Models;
using PagedList;

namespace FizzBuzzApp.Controllers
{
    /// <summary>
    /// Controller
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Called Interface reference from IFizzBuzzAppInfra
        /// </summary>
        /// 
        private readonly IFizzBuzzAppInfra fizzBuzzAppInfra;

        /// <summary>
        /// HomeController
        /// </summary>
        /// <param name="appInfra">appInfra</param>
        public HomeController(IFizzBuzzAppInfra appInfra)
        {
            fizzBuzzAppInfra = appInfra;
        }

        /// <summary>
        /// Return to Home/Index.
        /// </summary>
        /// <returns>Home Page</returns>
        public ActionResult Index()
        {
            return View(new FizzBuzzModel());
        }

        /// <summary>
        ///To display 20 values at a time so taking size with value 20.
        ///To do pagination intialising pagenumber and default value with 1.
        /// In Model state checking the values are less than 1000 and Greater than Zero
        /// In GetFizzBuzz it will check the DateTime based on Wednesday and NotWednesday and provide the required output.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="page"></param>
        /// <returns>It Returns Required output based on Conditions</returns>
        [HttpPost]
        public ViewResult Index(FizzBuzzModel model, int? page)
        {
            int size = 20;
            int pagenumber = page ?? 1;

            if (ModelState.IsValid)
            {
                model.FizzBuzzNumbers = fizzBuzzAppInfra.GetFizzBuzz(model.Number).ToPagedList(pagenumber, size);

                return View("FizzBuzz", model);
            }

            return View(model);
        }

        /// <summary>
        /// Taken ViewBag to store the Response message provide in GetFizzBuzz in the fizzBuzzAppInfra.
        ///To display 20 values at a time so taking size with value 20.
        ///To do pagination intialising pagenumber and default value with 1.
        /// In GetFizzBuzz it will check the DateTime based on Wednesday and NotWednesday and provide the required output.
        /// If the Given value is More than 1001 it will thrown an error which i am checking in true or false condition in fizzBuzzAppInfra.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="page"></param>
        /// <returns>Return Error Message if given value is greater than 1000 else it will return based on Wednesday or NotWednesday and provide the required output</returns>
        [HttpGet]
        public ViewResult FizzBuzz(int value, int? page)
        {
            ViewBag.errorMsg = "";
            int size = 20;
            int pagenumber = page ?? 1;

            var model = new FizzBuzzModel { Number = value };

            model.FizzBuzzNumbers = fizzBuzzAppInfra.GetFizzBuzz(model.Number).ToPagedList(pagenumber, size);

            if (fizzBuzzAppInfra.IsError)
            {
                ViewBag.errorMsg = fizzBuzzAppInfra.ResponseMessage;
            }

            return View(model);
        }
    }
}
