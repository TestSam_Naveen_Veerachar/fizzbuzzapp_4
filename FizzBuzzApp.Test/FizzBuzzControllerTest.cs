﻿using NUnit.Framework;
using FizzBuzzApp.Controllers;
using Moq;
using FizzBuzzApp.Infrastructure.Rules.Interface;
using System.Collections.Generic;
using System.Web.Mvc;
using FizzBuzzApp.Models;
using FluentAssertions;
using PagedList;

namespace FizzBuzzApp.Test
{
    /// <summary>
    /// Fizz Buzz Controller Test Class
    /// </summary>
    [TestFixture]
    public class FizzBuzzControllerTest
    {
        /// <summary>
        /// HomeController.
        /// </summary>
        private HomeController fizzBuzzControllerTest;

        /// <summary>
        /// Mock FizzBuzz App infra.
        /// </summary>
        private Mock<IFizzBuzzAppInfra> mockFizzBuzzAppInfra;

        /// <summary>
        /// FizzBuzz View.
        /// </summary>
        private FizzBuzzModel fizzBuzzModel;

        /// <summary>
        /// Configuration Method.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            mockFizzBuzzAppInfra = new Mock<IFizzBuzzAppInfra>();
            fizzBuzzControllerTest = new HomeController(mockFizzBuzzAppInfra.Object);
        }

        /// <summary>
        /// TestFor FizzBuzzResult With Valid Input Return FizzBuzz Series
        /// </summary>
        [Test]
        [TestCase(5, 1)]
        public void TestFor_FizzBuzz_View(int number, int pageNumber)
        {
            //Arrange
            var fizzBuzzList = new List<string> { "1", "2", "Fizz", "4", "Buzz" };
            mockFizzBuzzAppInfra.Setup(x => x.GetFizzBuzz(number)).Returns(fizzBuzzList);
            var fizzBuzzModel = new FizzBuzzModel
            {
                Number = number,
                FizzBuzzNumbers = fizzBuzzList.ToPagedList(pageNumber, 20)
            };
            //Act
            fizzBuzzControllerTest = new HomeController(mockFizzBuzzAppInfra.Object);
            //Assert
            var viewResult = fizzBuzzControllerTest.Index(fizzBuzzModel, pageNumber);

            viewResult.Should().BeOfType<ViewResult>();
        }

        /// <summary>
        /// TestFor FizzBuzzResult With Valid Input Return FizzBuzz Series
        /// </summary>
        [Test]
        [TestCase(25,2)]
        public void ReturnFizzBuzzSeries_forPagination2(int number, int pageNumber)
        {
            // Arrange.
            var list = new List<string>() 
            { 
                "1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", 
                "13", "14", "FizzBuzz", "16", "17", "Fizz", "19", "Buzz", "Fizz", "22", "23",
                "Fizz", "Buzz" 
            };

            mockFizzBuzzAppInfra.Setup(a => a.GetFizzBuzz(It.IsAny<int>())).Returns(list);

            // Act.
            fizzBuzzControllerTest = new HomeController(mockFizzBuzzAppInfra.Object);

            var viewResult = fizzBuzzControllerTest.FizzBuzz(number,2 );

            //Assert
            viewResult.Should().BeOfType<ViewResult>();
        }

        /// <summary>
        /// TestFor FizzBuzzResult With Valid Input Return FizzBuzz Series
        /// </summary>
        [Test]
        [TestCase(25,1)]
        public void ReturnFizzBuzzSeriesFor_Pagination1(int number, int pageNumber)
        {
            // Arrange.
            var list = new List<string>
            { 
                "1", "2", "Fizz", "4", "Buzz", "Fizz",
                "7", "8", "Fizz", "Buzz", "11", "Fizz", "13",
                "14", "FizzBuzz", "16", "17", "Fizz", "19",
                "Buzz", "Fizz", "22", "23", "Fizz", "Buzz" 
            };

            mockFizzBuzzAppInfra.Setup(a => a.GetFizzBuzz(It.IsAny<int>())).Returns(list);

            var fizzBuzzModel = new FizzBuzzModel { Number = 25 };
            
            // Act.
            var result = fizzBuzzControllerTest.Index(fizzBuzzModel, 1);
            var resultModel = (FizzBuzzModel)result.ViewData.Model;

            //Assert.
            resultModel.FizzBuzzNumbers.Should().HaveCount(20);
            mockFizzBuzzAppInfra.Verify(a => a.GetFizzBuzz(It.IsAny<int>()),Times.Once());
           
        }

        /// <summary>
        /// TestFor FizzBuzzResult With Valid Input Return FizzBuzz Series
        /// </summary>
        [Test]
        public void ReturnModelWhenInputIs2()
        {
            // Arrange
            fizzBuzzModel = new FizzBuzzModel();

            mockFizzBuzzAppInfra.Setup(t => t.GetFizzBuzz(It.IsAny<int>())).Returns(new List<string>()
            {
                " 1", " 2"
            });

            // Act
            var result = fizzBuzzControllerTest.Index(fizzBuzzModel, 1);

            // Assert
            result.Should().BeOfType<ViewResult>();
        }

        /// <summary>
        /// TestFor FizzBuzzResult With Valid Input Return FizzBuzz Series
        /// </summary>
        [Test]
        [TestCase(3)]
        public void TestFor_FizzBuzzResult_WithValidInput_ReturnFizzBuzzSeries(int number)
        {
            // Arrange.
            mockFizzBuzzAppInfra.Setup(a => a.GetFizzBuzz(It.IsAny<int>())).Returns(new List<string> { "1", "2", "Fizz" });

            var fizzBuzzModel = new FizzBuzzModel { Number = 3 };

            // Act.
            var result = fizzBuzzControllerTest.Index(fizzBuzzModel, 1);


            //Assert
            result.Should().BeOfType<ViewResult>();
        }
    }
}
