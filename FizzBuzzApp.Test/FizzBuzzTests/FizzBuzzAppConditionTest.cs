﻿
using System.Collections.Generic;
using FizzBuzzApp.Infrastructure;
using FizzBuzzApp.Infrastructure.Rules;
using FizzBuzzApp.Infrastructure.Rules.DayandWeekConditions.Def;
using FizzBuzzApp.Infrastructure.Rules.DayandWeekConditions.Interface;
using FizzBuzzApp.Infrastructure.Rules.FizzBuzzRules.Def;
using Moq;
using NUnit.Framework;

namespace FizzBuzzApp.Test.FizzBuzzTests
{
    /// <summary>
    /// FizzBuzz App Condition Class
    /// </summary>
    public class FizzBuzzAppConditionTest
    {
        /// <summary>
        /// FizzBuzz App Infra.
        /// </summary>
        private FizzBuzzAppInfra fizzBuzzAppInfra;

        /// <summary>
        /// Mock Fizz Condition.
        /// </summary>
        private Mock<IDivisible> mockFizzCondition;

        /// <summary>
        /// Mock Buzz Condition.
        /// </summary>
        private Mock<IDivisible> mockBuzzCondition;

        /// <summary>
        /// Mock FizzBUzz Condition.
        /// </summary>
        private Mock<IDivisible> mockFizzBuzzCondition;

        /// <summary>
        /// Divisible List.
        /// </summary>
        private IList<IDivisible> listOfDivisibles;


        private Mock<IDayOfTheWeekCondition> mockDayOfTheWeek;

        private Mock<IDayOfTheWeek> mockToday;

        private IList<IDayOfTheWeekCondition> dayOfTheWeekConditions;

        /// <summary>
        /// SetUp Method.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            listOfDivisibles = new List<IDivisible>();
            dayOfTheWeekConditions = new List<IDayOfTheWeekCondition>();
            mockFizzCondition = new Mock<IDivisible>();
            mockBuzzCondition = new Mock<IDivisible>();
            mockFizzBuzzCondition = new Mock<IDivisible>();
            mockToday = new Mock<IDayOfTheWeek>();
            mockDayOfTheWeek = new Mock<IDayOfTheWeekCondition>();
            dayOfTheWeekConditions.Add(mockDayOfTheWeek.Object);
            fizzBuzzAppInfra = new FizzBuzzAppInfra(listOfDivisibles, dayOfTheWeekConditions);
        }

        /// <summary>
        /// Test for Get Fizz Buzz Series for InValid Inputs.
        /// </summary>
        [Test]
        public void GetFizzBuzzSeries_WithReturnEmptyList()
        {
            // Arrange
            var expectedResult = new List<string>();

            // Act.
            var resultList = fizzBuzzAppInfra.GetFizzBuzz(0);

            // Assert.
            Assert.AreEqual(expectedResult, resultList);
        }

        /// <summary>
        /// Test Get Fizz Series When Input Is Three.
        /// </summary>
        [Test]
        public void GetFizzBuzzSeries_WithInputIsThree_ReturnFizz()
        {
            // Arrange.

            mockToday.Setup(a => a.IsDayOfWeek(FizzBuzzLogic.Fizz)).Returns(false);
            mockFizzCondition.Setup(a => a.DivideNumber(3)).Returns("Fizz");

            listOfDivisibles.Add(mockFizzCondition.Object);
            mockDayOfTheWeek.Setup(x => x.DayOfTheWeekCondition(It.IsAny<string>())).Returns("Fizz");

            // Act.
            var resultList = fizzBuzzAppInfra.GetFizzBuzz(3);

            // Assert.
            Assert.NotNull(resultList);
            Assert.That(resultList, Has.Count.EqualTo(3));
        }

        /// <summary>
        /// Test Get Wizz  Series When Input Is Three.
        /// </summary>
        [Test]
        public void GetFizzBuzzSeries_WithInputIsThree_ReturnWizz()
        {
            // Arrange.

            mockToday.Setup(a => a.IsDayOfWeek(FizzBuzzLogic.Fizz)).Returns(true);
            mockFizzCondition.Setup(a => a.DivideNumber(3)).Returns("Wizz");

            listOfDivisibles.Add(mockFizzCondition.Object);
            mockDayOfTheWeek.Setup(x => x.DayOfTheWeekCondition(It.IsAny<string>())).Returns("Wizz");

            // Act.
            var resultList = fizzBuzzAppInfra.GetFizzBuzz(3);

            // Assert.
            Assert.NotNull(resultList);
            Assert.That(resultList, Has.Count.EqualTo(3));
        }

        /// <summary>
        /// Test Get Buzz Series When Input Is Five
        /// </summary>
        [Test]
        public void GetFizzBuzzSeries_WithInputIsFive_ReturnBuzz()
        {
            // Arrange.
            mockToday.Setup(a => a.IsDayOfWeek(FizzBuzzLogic.Buzz)).Returns(false);
            mockBuzzCondition.Setup(a => a.DivideNumber(5)).Returns("Buzz");

            listOfDivisibles.Add(mockBuzzCondition.Object);
            mockDayOfTheWeek.Setup(x => x.DayOfTheWeekCondition(It.IsAny<string>())).Returns("Buzz");

            // Act.
            var resultList = fizzBuzzAppInfra.GetFizzBuzz(5);

            // Assert.
            Assert.NotNull(resultList);
            Assert.That(resultList, Has.Count.EqualTo(5));

        }


        /// <summary>
        /// Test Get Wuzz  When Input Is five.
        /// </summary>
        [Test]
        public void TestFor_GetFizzBuzzSeries_WithInputIsFive_ReturnWuzz()
        {
            // Arrange.
            mockToday.Setup(a => a.IsDayOfWeek(FizzBuzzLogic.Wuzz)).Returns(false);
            mockBuzzCondition.Setup(a => a.DivideNumber(5)).Returns("Wuzz");

            listOfDivisibles.Add(mockBuzzCondition.Object);
            mockDayOfTheWeek.Setup(x => x.DayOfTheWeekCondition(It.IsAny<string>())).Returns("Wuzz");

            // Act.
            var resultList = fizzBuzzAppInfra.GetFizzBuzz(5);

            // Assert.
            Assert.NotNull(resultList);
            Assert.That(resultList, Has.Count.EqualTo(5));
        }
        /// <summary>
        /// Test Get Fizz Buzz Series When Input Is Fifteen.
        /// </summary>
        [Test]
        public void TestFor_GetFizzBuzzSeries_WithInputIsFifteen_ReturnFizzBuzz()
        {
            // Arrange.

            mockToday.Setup(a => a.IsDayOfWeek(FizzBuzzLogic.FizzBuzz)).Returns(false);
            mockFizzBuzzCondition.Setup(a => a.DivideNumber(15)).Returns("Fizz Buzz");
            mockDayOfTheWeek.Setup(x => x.DayOfTheWeekCondition(It.IsAny<string>())).Returns("Fizz Buzz");

            listOfDivisibles.Add(mockFizzBuzzCondition.Object);

            // Act.
            var resultList = fizzBuzzAppInfra.GetFizzBuzz(15);

            // Assert.
            Assert.NotNull(resultList);
            Assert.That(resultList, Has.Count.EqualTo(15));
        }

        /// <summary>
        /// Test Get Wizz Wuzz When Input Is Fifteen.
        /// </summary>
        [Test]
        public void TestFor_GetFizzBuzzSeries_WithInputIsFifteen_ReturnizzWuzz()
        {
            // Arrange.
            mockToday.Setup(a => a.IsDayOfWeek(FizzBuzzLogic.WizzWuzz)).Returns(false);
            mockFizzBuzzCondition.Setup(a => a.DivideNumber(15)).Returns("Wizz Wuzz");
            mockDayOfTheWeek.Setup(x => x.DayOfTheWeekCondition(It.IsAny<string>())).Returns("Wizz Wuzz");
            listOfDivisibles.Add(mockFizzBuzzCondition.Object);

            // Act.
            var resultList = fizzBuzzAppInfra.GetFizzBuzz(15);

            // Assert.
            Assert.NotNull(resultList);
            Assert.That(resultList, Has.Count.EqualTo(15));
        }
    }
}