﻿namespace FizzBuzzApp.Test.FizzBuzzTests
{
    using Infrastructure.Rules.FizzBuzzRules.FizzBuzzConditions;
    using NUnit.Framework;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Infrastructure.Rules.FizzBuzzRules.Def;

    /// <summary>
    /// Fizz and Buzz Condition class
    /// </summary>
    [TestClass]
    public class FizzAndBuzzConditionTest
    {
        /// <summary>
        /// Divisible Object
        /// </summary>
        private IDivisible fizzAndBuzzCondition;

        /// <summary>
        /// SetUpMethod
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            fizzAndBuzzCondition = new FizzAndBuzz();
        }

        /// <summary>
        /// Return FizzBuzz
        /// </summary>
        [Test]
        public void FizzAndBuzz_ForInput15()
        {
            //Arrange

            int input = 15;

            //Act

            var result = fizzAndBuzzCondition.DivideNumber(input);

            //Assert

            NUnit.Framework.Assert.AreEqual("Fizz Buzz", result);
        }

        /// <summary>
        /// To return null if not divisible by 3 and 5.
        /// </summary>
        [Test]
        public void FizzAndBuzz_ForInput2()
        {
            //Arrange
            int input = 2;

            //Act
            var result = fizzAndBuzzCondition.DivideNumber(input);

            //Assert
            NUnit.Framework.Assert.AreEqual(string.Empty, result);
        }
    }
}
