﻿namespace FizzBuzzApp.Test.FizzBuzzTests
{
    using Infrastructure.Rules.FizzBuzzRules.FizzBuzzConditions;
    using NUnit.Framework;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Infrastructure.Rules.FizzBuzzRules.Def;

    /// <summary>
    /// Fizz Condition Test Class
    /// </summary>
    [TestClass]
    public class FizzConditionTest
    {
        /// <summary>
        /// Divisible Object
        /// </summary>
        private IDivisible fizzCondition;

        /// <summary>
        /// SetUp Method.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            fizzCondition = new FizzCondition();
        }

        /// <summary>
        /// To return Fizz
        /// </summary>
        [Test]
        public void FizzTest_ForInput3()
        {
            //Arrange
            int input = 3;

            //Act
            var result = fizzCondition.DivideNumber(input);

            //Assert

            NUnit.Framework.Assert.AreEqual("Fizz", result);
        }

        /// <summary>
        /// To return null if not divisible by 3 and 5.
        /// </summary>
        [Test]
        public void FizzTest_ForInput2()
        {
            //Arrange
            int input = 2;

            //Act
            var result = fizzCondition.DivideNumber(input);

            //Assert
            NUnit.Framework.Assert.AreEqual(string.Empty, result);
        }
    }
}
