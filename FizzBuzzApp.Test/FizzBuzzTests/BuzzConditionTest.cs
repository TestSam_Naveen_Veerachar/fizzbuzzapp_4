﻿namespace FizzBuzzApp.Test.FizzBuzzTests
{
    using Infrastructure.Rules.FizzBuzzRules.FizzBuzzConditions;
    using NUnit.Framework;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Infrastructure.Rules.FizzBuzzRules.Def;

    /// <summary>
    /// Buzz condition class
    /// </summary>
    [TestClass]
    public class BuzzConditionTest
    {
        /// <summary>
        /// Divisible Object
        /// </summary>
        private IDivisible buzzCondition;

        /// <summary>
        /// SetUp Method
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            buzzCondition = new BuzzCondition();
        }

        /// <summary>
        /// To return Buzz.
        /// </summary>
        [Test]
        public void BuzzTest_ForInput5()
        {
            //Arrang
            int input = 5;

            //Act
            var result = buzzCondition.DivideNumber(input);

            //Assert
            NUnit.Framework.Assert.AreEqual("Buzz", result);
        }

        /// <summary>
        /// To return null if not divisible by 5.
        /// </summary>
        [Test]
        public void BuzzTest_ForInput2()
        {
            //Arrange
            int input = 2;

            //Act
            var result = buzzCondition.DivideNumber(input);

            //Assert
            NUnit.Framework.Assert.AreEqual(string.Empty, result);
        }

    }
}
