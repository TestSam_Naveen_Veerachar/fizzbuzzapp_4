﻿using System.Collections.Generic;
using System.Linq;
using FizzBuzzApp.Infrastructure.Rules.DayandWeekConditions.Def;
using FizzBuzzApp.Infrastructure.Rules.FizzBuzzRules.Def;
using FizzBuzzApp.Infrastructure.Rules.Interface;


namespace FizzBuzzApp.Infrastructure.Rules
{
    /// <summary>
    /// FizzBuzzAppInfra Class.
    /// </summary>
    public class FizzBuzzAppInfra : IFizzBuzzAppInfra
    {
        /// <summary>
        /// List of Divisible Conditions.
        /// </summary>
        private readonly IList<IDivisible> conditions;

        /// <summary>
        /// List of day of week condition.
        /// </summary>
        private readonly IList<IDayOfTheWeekCondition> dayOfTheWeek;

        /// <summary>
        /// Calling from IFizzBuzzAppInfra Repository.
        /// To compare the todays date is wednesday or not.
        /// </summary>
        public FizzBuzzAppInfra(IList<IDivisible> conditions, IList<IDayOfTheWeekCondition> dayOfTheWeek)
        {
            this.conditions = conditions;
            this.dayOfTheWeek = dayOfTheWeek;
        }

        /// <summary>
        /// To check the condition True or False.
        /// </summary>
        public bool IsError { get; set; }

        /// <summary>
        /// To show the error response.
        /// </summary>
        public string ResponseMessage { get; set; }

        /// <summary>
        /// Get the number eneter number in the textbox to validate if it is greater than 1000 throw exception.
        /// Else check which are divisible by 3,5 and both 3,5
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public IEnumerable<string> GetFizzBuzz(int value)
        {
            var numbers = new List<string>();
            if (value < 1 || value > 1000)
            {
                IsError = true;
                ResponseMessage = "The Value should not be less than 0 and more than 1000";
                return numbers;
            }
            for (var i = 1; i <= value; i++)
            {
                var matchedStrategy = conditions.FirstOrDefault(x => x.DivideNumber(i) != string.Empty);

                var finalOutput = string.Empty;

                if (matchedStrategy != null)
                {
                    var matchedOutput = matchedStrategy.DivideNumber(i);

                    if (!string.IsNullOrEmpty(matchedOutput))
                    {
                        finalOutput = VerifyTheDayOfTheWeek(matchedOutput);
                    }
                }

                numbers.Add(finalOutput.Length > 0 ? finalOutput : i.ToString());
            }
            return numbers;
        }

        /// <summary>
        /// VerifyTheDay Method.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>The matched result.</returns>
        public string VerifyTheDayOfTheWeek(string input)
        {
            var matchingStrategy = dayOfTheWeek.FirstOrDefault(y => y.DayOfTheWeekCondition(input) != string.Empty);
            var matchedResult = string.Empty;

            if (matchingStrategy != null)
            {
                matchedResult = matchingStrategy.DayOfTheWeekCondition(input);
            }

            return matchedResult;
        }
    }
}
