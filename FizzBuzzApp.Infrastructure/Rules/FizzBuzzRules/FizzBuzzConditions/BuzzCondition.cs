﻿namespace FizzBuzzApp.Infrastructure.Rules.FizzBuzzRules.FizzBuzzConditions
{
    using Def;

    /// <summary>
    /// BuzzCondition class
    /// </summary>
    public class BuzzCondition : IDivisible
    {
        /// <summary>
        /// Buzz
        /// </summary>
        /// <param name="number">Input parameter</param>
        /// <returns>If it is divisible by 5</returns>
        public string DivideNumber(int number)
        {
            var result = string.Empty;

            if (number % 5 == 0 && number % 3 != 0)
            {
                result = FizzBuzzLogic.Buzz;
            }

            return result;
        }
    }
}
