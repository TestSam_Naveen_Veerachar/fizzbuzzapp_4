﻿namespace FizzBuzzApp.Infrastructure.Rules.FizzBuzzRules.FizzBuzzConditions
{
    using Def;

    /// <summary>
    /// Fizz Condition class
    /// </summary>
    public class FizzCondition : IDivisible
    {
        /// <summary>
        /// Fizz
        /// </summary>
        /// <param name="number">The Number Parameter</param>
        /// <returns>If it is divisible by 3</returns>
        public string DivideNumber(int number)
        {
            var result = string.Empty;

            if (number % 3 == 0 && number % 5 != 0)
            {
                result = FizzBuzzLogic.Fizz;
            }

            return result;
        }
    }
}
