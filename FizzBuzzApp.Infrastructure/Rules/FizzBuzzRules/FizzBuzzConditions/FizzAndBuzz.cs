﻿namespace FizzBuzzApp.Infrastructure.Rules.FizzBuzzRules.FizzBuzzConditions
{
    using Def;

    /// <summary>
    /// FizzAndBuzz class
    /// </summary>
    public class FizzAndBuzz : IDivisible
    {
        /// <summary>
        /// FizzBuzz
        /// </summary>
        /// <param name="number">The input parameter</param>
        /// <returns>If it is divisible by 3 and 5</returns>
        public string DivideNumber(int number)
        {
            var result = string.Empty;

            if (number % 3 == 0 && number % 5 == 0)
            {
                result = FizzBuzzLogic.FizzBuzz;
            }

            return result;
        }
    }
}
