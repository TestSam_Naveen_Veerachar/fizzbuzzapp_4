﻿namespace FizzBuzzApp.Infrastructure.Rules.FizzBuzzRules.Def
{
    /// <summary>
    /// Interface for Divide Number condition class
    /// </summary>
    public interface IDivisible
    {

        /// <summary>
        /// Divide Number
        /// </summary>
        /// <param name="number"></param>
        /// <returns>Number</returns>
        string DivideNumber(int number);
    }
}

