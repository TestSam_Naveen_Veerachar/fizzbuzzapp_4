﻿namespace FizzBuzzApp.Infrastructure.Rules.DayandWeekConditions.Def
{
    /// <summary>
    /// Day of the week condition.
    /// </summary>
    public interface IDayOfTheWeekCondition
    {
        /// <summary>
        /// Day of the week.
        /// </summary>
        /// <param name="input">The input</param>
        /// <returns>string</returns>
        string DayOfTheWeekCondition(string input);
    }
}
