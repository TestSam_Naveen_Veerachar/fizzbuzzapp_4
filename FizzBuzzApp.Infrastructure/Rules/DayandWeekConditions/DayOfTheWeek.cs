﻿using System;
using FizzBuzzApp.Infrastructure.Rules.DayandWeekConditions.Interface;

namespace FizzBuzzApp.Infrastructure.Rules.DayandWeekConditions
{
    /// <summary>
    /// DayOfTheWeek Class.
    /// </summary>
    public class DayOfTheWeek : IDayOfTheWeek
    {
        /// <summary>
        /// Is Day Of Week .
        /// </summary>
        /// <param name="dayOfWeek">The  Day Of Week.</param>
        /// <returns>If it is  Wednesday Of Week or not.</returns>
        public bool IsDayOfWeek(string dayOfWeek)
        {
            return DateTime.Today.DayOfWeek.ToString() == dayOfWeek;
        }
    }
}
