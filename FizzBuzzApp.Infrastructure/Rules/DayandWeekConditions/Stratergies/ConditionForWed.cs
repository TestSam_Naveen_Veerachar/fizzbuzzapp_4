﻿using FizzBuzzApp.Infrastructure.Rules.DayandWeekConditions.Def;
using FizzBuzzApp.Infrastructure.Rules.DayandWeekConditions.Interface;

namespace FizzBuzzApp.Infrastructure.Rules.DayandWeekConditions.Stratergies
{
    /// <summary>
    /// Conditions for wednesday.
    /// </summary>
    public class ConditionForWed : IDayOfTheWeekCondition
    {
        /// <summary>
        /// dayOfTheWeek.
        /// </summary>
        private readonly IDayOfTheWeek dayOfTheWeek;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConditionForWed"/> class.
        /// </summary>
        /// <param name="dayOfTheWeek">The day of week.</param>
        public ConditionForWed(IDayOfTheWeek dayOfTheWeek)
        {
            this.dayOfTheWeek = dayOfTheWeek;
        }

        /// <summary>
        /// Check if wednesday and returns the expected result.
        /// </summary>
        /// <param name="input">The input string.</param>
        /// <returns>result.</returns>
        public string DayOfTheWeekCondition(string input)
        {
            var result = string.Empty;

            if (dayOfTheWeek.IsDayOfWeek(FizzBuzzLogic.Wednesday))
            {
                if (input == FizzBuzzLogic.FizzBuzz)
                {
                    result = FizzBuzzLogic.WizzWuzz;
                }
                else if (input == FizzBuzzLogic.Buzz)
                {
                    result = FizzBuzzLogic.Wuzz;
                }
                else if (input == FizzBuzzLogic.Fizz)
                {
                    result = FizzBuzzLogic.Wizz;
                }
            }
            else
            {
                result = input;
            }

            return result;
        }
    }
}

