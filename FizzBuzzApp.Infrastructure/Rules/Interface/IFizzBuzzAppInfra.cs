﻿using System.Collections.Generic;

namespace FizzBuzzApp.Infrastructure.Rules.Interface
{
    /// <summary>
    /// Interface for fizz buzz  rule class.
    /// </summary>
    public interface IFizzBuzzAppInfra
    {
        /// <summary>
        /// Error condition
        /// </summary>
        bool IsError { get; set; }

        /// <summary>
        /// ResponseMessage
        /// </summary>
        string ResponseMessage { get; set; }

        /// <summary>
        /// Gets the fizz buzz display list.
        /// </summary>
        /// <param name="value">The value</param>
        /// <returns>Return Fizz Buzz Series</returns>
        IEnumerable<string> GetFizzBuzz(int value);
    }
}
