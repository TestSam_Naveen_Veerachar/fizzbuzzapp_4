﻿namespace FizzBuzzApp.Infrastructure
{
    /// <summary>
    /// Application Constant Class
    /// </summary>
    public class FizzBuzzLogic
    {
        /// <summary>
        /// Day of week
        /// </summary>
        public const string Wednesday = "Wednesday";

        /// <summary>
        /// Fizz
        /// </summary>
        public const string Fizz = "Fizz";

        /// <summary>
        /// Buzz
        /// </summary>
        public const string Buzz = "Buzz";

        /// <summary>
        /// FizzBuzz
        /// </summary>
        public const string FizzBuzz = "Fizz Buzz";

        /// <summary>
        /// Wizz
        /// </summary>
        public const string Wizz = "Wizz";

        /// <summary>
        /// Wuzz
        /// </summary>
        public const string Wuzz = "Wuzz";

        /// <summary>
        /// WizzWuzz
        /// </summary>
        public const string WizzWuzz = "Wizz Wuzz";
    }
}
